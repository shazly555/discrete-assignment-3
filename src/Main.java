import Questions.CRT;
import Questions.ExtendedEculid;

import java.math.BigInteger;
import java.util.Random;

/**
 * Created by khaledabdelfattah on 12/7/17.
 */
public class Main {

    static void sieveOfEratosthenes(long n)
    {
        // Create a boolean array "prime[0..n]" and initialize
        // all entries it as true. A value in prime[i] will
        // finally be false if i is Not a prime, else true.
        boolean prime[] = new boolean[(int) n+1];
        for(int i=0;i<n;i++)
            prime[i] = true;

        for(int p = 2; p*p <=n; p++)
        {
            // If prime[p] is not changed, then it is a prime
            if(prime[p] == true)
            {
                // Update all multiples of p
                for(int i = p*2; i <= n; i += p)
                    prime[i] = false;
            }
        }

        // Print all prime numbers
        for(int i = 2; i <= n; i++)
        {
            if(prime[i] == true)
                System.out.print(i + " ");
        }
    }

    public static void main(String[] args) {
        int[] primes = {277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389,
                397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541};
        System.out.println(ExtendedEculid.modInverse(17, 43));
        BigInteger[] a = {new BigInteger("2"), new BigInteger("3"), new BigInteger("2")},
                m = {new BigInteger("3"), new BigInteger("5"), new BigInteger("7")};

        System.out.println(CRT.findAnswer(a, m));
        a = new BigInteger[5];
        BigInteger b;
        for (int i = 0; i < 5; i++) {
            b  = new BigInteger(128, new Random());
            a[i] = b;
            System.out.print("new BigInteger(\"" + b.longValue() + "\"), ");
        }
        System.out.println();
        for (int i = 0; i < 5; i++) {
            b  = new BigInteger(128, new Random());
            while (b.compareTo(a[i]) == 1)
                b  = new BigInteger(128, new Random());
            System.out.print("new BigInteger(\"" + b.longValue() + "\"), ");
        }
    }
}
