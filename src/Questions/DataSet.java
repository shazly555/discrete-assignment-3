package Questions;

import java.math.BigInteger;
import java.util.Random;

/**
 * Created by khaledabdelfattah on 12/9/17.
 */
public class DataSet {
    public void setData() {
        int[] primes = {277, 281, 283, 293, 307, 311, 313, 317, 331, 337, 347, 349, 353, 359, 367, 373, 379, 383, 389,
                397, 401, 409, 419, 421, 431, 433, 439, 443, 449, 457, 461, 463, 467, 479, 487, 491, 499, 503, 509, 521, 523, 541};
        BigInteger[] a = new BigInteger[primes.length],
        m = new BigInteger[primes.length];
        BigInteger A = new BigInteger(64, new Random());
        for (int i = 0; i < primes.length; i++) {
            a[i] = A.divide(BigInteger.valueOf(primes[i]));
            m[i] = BigInteger.valueOf(primes[i]);
        }
        BigInteger z = CRT.findAnswer(a, m);
    }
}
