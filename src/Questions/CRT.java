package Questions;

import java.math.BigInteger;

/**
 * Created by khaledabdelfattah on 12/7/17.
 */
public class CRT {
    public static BigInteger findAnswer(BigInteger[] a, BigInteger[] m) {
        BigInteger z = new BigInteger("0");
        BigInteger M = new BigInteger("1");
        for (BigInteger i : m)
            M = M.multiply(i);
        for (int i = 0; i < a.length; i++) {
            BigInteger Mi = M.divide(m[i]);
            BigInteger modInverseOfMi = BigInteger.valueOf(ExtendedEculid.modInverse(Mi.intValue(), m[i].intValue()));
            z = z.add(a[i].multiply(Mi.multiply(modInverseOfMi)));
            z = z.mod(M);
        }
        return z;
    }
}
