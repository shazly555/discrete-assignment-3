package Questions;

/**
 * Created by khaledabdelfattah on 12/7/17.
 */
public class ExtendedEculid {

    public static int modInverse(int a, int mod) {
        int currentModule = mod, t, q;
        int x = 0, y = 1;
        if (mod == 1)
            return 0;
        while (a > 1) {
            q = a / currentModule;
            t = currentModule;
            currentModule = a % currentModule;
            a = t;
            t = x;
            x = y - q * x;
            y = t;
        }
        if (y < 0)
            y += mod;
        return y;
    }
}
